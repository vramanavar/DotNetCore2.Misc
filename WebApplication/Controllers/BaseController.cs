﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebApplication.Controllers
{
    public abstract class BaseController<TController> : Controller where TController : Controller
    {
        protected async Task<IActionResult> ExceptionHandledOperationAsync<T>(Func<Task<T>> operation)
        {
            try
            {
                return (IActionResult)await operation.Invoke();
            }
            catch (Exception exception)
            {
                //Log exception
                return StatusCode(500);
            }
        }
    }
}
