﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using WebApplication.Helpers;

namespace WebApplication.Controllers
{
    public class FileUploadController : BaseController<FileUploadController>
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile formFile)
        {
            return await ExceptionHandledOperationAsync<IActionResult>(async () =>
            {
                if (Request.Form.Files.Count == 0) throw new InvalidOperationException("File not supplied");

                // full path to file in temp location
                var filePath = Path.GetTempFileName();
                filePath = filePath.Replace(".tmp", ".xlsx");
                var file = Request.Form.Files[0];
                if (file.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                        // process uploaded files
                        // Don't rely on or trust the FileName property without validation.

                        var excelDataSet = ExcelHelper.GetWorksheetContentsFromExcelDataReader(stream);
                        var excelDataTable = excelDataSet.Tables[0];

                        var storeIdColumn = "StoreId";
                        var departmentIdColumn = "DepartmentId";
                        var categoryIdColumn = "CategoryId";
                        var subCategoryIdColumn = "SubCategoryId";
                        var segmentIdColumn = "SegmentId";

                        excelDataTable.Columns[0].ColumnName = storeIdColumn;
                        excelDataTable.Columns[1].ColumnName = departmentIdColumn;
                        excelDataTable.Columns[2].ColumnName = categoryIdColumn;
                        excelDataTable.Columns[3].ColumnName = subCategoryIdColumn;
                        excelDataTable.Columns[4].ColumnName = segmentIdColumn;

                        DataTableHelper dataTableHelper = new DataTableHelper(excelDataTable, new[] { storeIdColumn, departmentIdColumn, categoryIdColumn, subCategoryIdColumn, segmentIdColumn });
                        var countSummary = dataTableHelper.GetDuplicateRecordsCountSummary();
                        List<ValueObject> specificStoreOverrideValues = new List<ValueObject>();

                        foreach (DataRow dataRow in excelDataTable.Rows)
                        {
                            //NOTE: Each row value should be parsed and validated at this state; and should not allow to proceed the 
                            //below block of code

                            //Below code assumes; all row values are valid at this stage.
                            var duplicateKeyValue = dataRow[0].ToString() + dataRow[1] + dataRow[2] + dataRow[3] + dataRow[4];
                            if (!countSummary.ContainsKey(duplicateKeyValue))
                            {
                                if (string.IsNullOrWhiteSpace(dataRow[0].ToString()) || dataRow[0].ToString().Equals("0"))
                                {
                                    //TODO: All stores update for criteria matching - DeptId+CatId+SubCatId+SegmentId
                                }
                                else
                                {
                                    //Catpure all rows having StoreId based updates
                                    specificStoreOverrideValues.Add(new ValueObject()
                                    {
                                        StoreId = Convert.ToInt32(dataRow[0]),
                                        DepartmentId = Convert.ToInt32(dataRow[1]),
                                        CategoryId = Convert.ToInt32(dataRow[2]),
                                        SubCategoryId = Convert.ToInt32(dataRow[3]),
                                        SegmentId = Convert.ToInt32(dataRow[4])
                                    });
                                }
                            }
                            else
                            {
                                //TODO: Capture the data and log, saying that it is duplicated record
                            }
                        }

                        //TODO: Once all rows not having specific rows have been updated; take up specific store update values from specificStoreOverrideValues list and update
                        specificStoreOverrideValues.ForEach((item) =>
                        {
                            //TODO: Update for criteria matching -StoreId + DeptId + CatId + SubCatId + SegmentId
                        });
                        var x = countSummary;
                    }
                }
                return Ok();
            });
        }
    }

    public class ValueObject
    {
        public int StoreId { get; set; }
        public int DepartmentId { get; set; }
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public int SegmentId { get; set; }
    }
}