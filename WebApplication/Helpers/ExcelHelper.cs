﻿using ExcelDataReader;
using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;

namespace WebApplication.Helpers
{
    public static class ExcelHelper
    {
        public static void ReadFile(this string filePath)
        {
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {

                // Auto-detect format, supports:
                //  - Binary Excel files (2.0-2003 format; *.xls)
                //  - OpenXml Excel files (2007 format; *.xlsx)
                try
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        
                        do
                        {
                            while (reader.Read())
                            {
                                for(int index=0; index < reader.FieldCount; index++)
                                {
                                    Console.WriteLine($"Field-{index}: {reader[index]}");
                                }
                                Console.WriteLine(Enumerable.Repeat<String>("=", 50));
                            }
                        } while (reader.NextResult()); 

                        // 2. Use the AsDataSet extension method
                        var result = reader.AsDataSet();
                        Console.WriteLine($"Rows: {result.Tables[0].Rows.Count}");
                        foreach(var row in result.Tables[0].Rows)
                        {
                        }
                        // The result of each spreadsheet is in result.Tables
                    }
                }
                catch(Exception exception)
                {
                    Console.WriteLine(exception.ToString());
                }
            }
        }

        public static string GetFormattedValue(this IExcelDataReader reader, int columnIndex, CultureInfo culture)
        {
            var value = reader.GetValue(columnIndex);
            var formatString = reader.GetNumberFormatString(columnIndex);
            if (formatString != null)
            {
                //var format = new NumberFormat(formatString);
                //return format.Format(value, culture);
            }
            return Convert.ToString(value, culture);
        }

        public static DataSet GetWorksheetContentsFromExcelDataReader(Stream stream)
        {
            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                var result = reader.AsDataSet();

                return result;
            }
        }
    }
}
