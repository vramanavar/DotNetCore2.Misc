﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WebApplication.Helpers
{
    public class DataTableHelper
    {
        private readonly DataTable _sourceDataTable;
        private readonly string[] _columnNames;

        public DataTableHelper(DataTable sourceDataTable, string[] columnsNames)
        {
            _sourceDataTable = sourceDataTable ?? throw new ArgumentNullException(nameof(sourceDataTable));
            _columnNames = columnsNames ?? throw new ArgumentNullException(nameof(columnsNames));
            if (_columnNames.Length == 0) throw new InvalidOperationException("At least one columne name should be passed");
        }
        public DataTable GetDistinctRecords()
        {
            return _sourceDataTable.DefaultView.ToTable(true, _columnNames);
        }

        public Dictionary<string, int> GetDuplicateRecordsCountSummary()
        {
            var duplicateRecordsCount = new Dictionary<string, int>();
            var distinctRecordsTable = GetDistinctRecords();

            for(var rowIndex = 1; rowIndex < distinctRecordsTable.Rows.Count; rowIndex++)
            {
                var dataRow = distinctRecordsTable.Rows[rowIndex];
                var sbCriteria = new StringBuilder();
                var key = new StringBuilder();
                foreach (var columnName in _columnNames)
                {
                    key.Append(dataRow[columnName]);
                    if (dataRow[columnName].ToString().Length > 0)
                    {
                        sbCriteria.Append("ISNULL(" + columnName + ", ' ') = '" + dataRow[columnName] + "' AND ");
                    }
                    else
                    {
                        sbCriteria.Append("CONVERT(ISNULL(" + columnName + ", ''), 'System.String') = '' AND ");
                    }
                }
                _sourceDataTable.DefaultView.RowFilter = sbCriteria.ToString().Substring(0, sbCriteria.ToString().Length - 5);
                if (_sourceDataTable.DefaultView.Count > 1)
                {
                    duplicateRecordsCount.Add(key.ToString(), _sourceDataTable.DefaultView.Count);
                }
            }
            return duplicateRecordsCount;
        }
    }
}
